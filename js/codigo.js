var t={
    "trs":1,
    "ths":[
        "#",
        "No. de control",
        "Nombre",
        "Alguna cosa"
        ]
    };

var d={
    "tds":[
        [1,"10160844","Jimenez Alvarez Jasiel"],
        [2,"10161515","Vasquez Pinacho Magaly"],
        [3,"10160000","Diaz Jimenez Yannik","La cosa"],
        [4,"10165555","Ruth Vilma Carreño Diaz"],
        [5,"10162222","Sosososos  vaca"]
        ]
    };
    


function creaTabla(t,d){
	var tabla=document.createElement("table");
	tabla.setAttribute("id","tablaD");
	//tabla
	var cabecera=document.createElement("thead");
	var tr1=document.createElement("tr");
	var cuerpo=document.createElement("tbody");
	//var tr2=document.createElement("tr");
	tabla.appendChild(cabecera).setAttribute("id","cabecera");
	cabecera.appendChild(tr1); 
	
	for(var i=0;i<t.ths.length;i++)
	{
	  var th= document.createElement("th");
      th.textContent = t.ths[i];
      th.style.backgroundColor="gray";
	  tr1.appendChild(th);
	}
	
	tabla.appendChild(cuerpo).setAttribute("id","cuerpo");
	
	for(var j=0;j<d.tds.length;j++){
	var tr=document.createElement("tr");
	for(var k=0;k<d.tds[j].length;k++){
	var td=document.createElement("td");
	td.textContent=d.tds[j][k];
	if(j%2>0){
	td.style.backgroundColor="green";
	}
    else{
	td.style.backgroundColor="yellow";
	}
	tr.appendChild(td);
	}
	cuerpo.appendChild(tr);
	}
	tabla.style.backgroundColor="white";
	tabla.style.borderColor="black";
	tabla.style.borderWidth="2px";
	tabla.style.borderStyle="dotted";
	
	document.body.appendChild(tabla);
}

    
creaTabla(t,d);